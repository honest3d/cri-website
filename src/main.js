import Vue from 'vue';
import App from './App.vue';
import VueKonva from 'vue-konva';
import router from './router';
import VueParticles from 'vue-particles';
import VueAnime from 'vue-animejs';
import Moment from 'vue-moment';
const mouse = Vue.observable({mouse:[]});
Vue.config.productionTip = false;
Vue.use(VueKonva);
Vue.use(VueParticles);
Vue.use(VueAnime);
Vue.use(Moment);
Object.defineProperty(Vue.prototype, '$mouse', {
    get () {
        return mouse.mouse;
    },
  
    set (value) {
        mouse.mouse = value;
    }
});

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
